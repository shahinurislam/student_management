-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Sep 20, 2017 at 11:12 PM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `test2`
--

-- --------------------------------------------------------

--
-- Table structure for table `all_info`
--

CREATE TABLE IF NOT EXISTS `all_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `student_id` varchar(255) NOT NULL,
  `cour_title` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `all_info`
--

INSERT INTO `all_info` (`id`, `student_id`, `cour_title`) VALUES
(5, 'S1505238036', 'English, Math'),
(6, 'S1505230724', 'English');

-- --------------------------------------------------------

--
-- Table structure for table `cour_form`
--

CREATE TABLE IF NOT EXISTS `cour_form` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `code` varchar(255) NOT NULL,
  `duration` varchar(255) NOT NULL,
  `creadit` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `department` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;

--
-- Dumping data for table `cour_form`
--

INSERT INTO `cour_form` (`id`, `title`, `code`, `duration`, `creadit`, `description`, `department`) VALUES
(13, 'Bangla', 'B-22', '19-12-2017', '3', 'Bngla Book', 'CSE'),
(14, 'English', 'E-88', '14-7-1913', '4', 'English Book', 'EEE'),
(15, 'Math', 'M-66', '1-1-1901', '5', 'math book', 'BBA');

-- --------------------------------------------------------

--
-- Table structure for table `user_info`
--

CREATE TABLE IF NOT EXISTS `user_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `student_id` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `pass` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `number` varchar(255) NOT NULL,
  `gender` varchar(255) NOT NULL,
  `skill` varchar(255) NOT NULL,
  `hobbies` varchar(255) NOT NULL,
  `dob` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `student_id` (`student_id`),
  KEY `student_id_2` (`student_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=56 ;

--
-- Dumping data for table `user_info`
--

INSERT INTO `user_info` (`id`, `student_id`, `name`, `email`, `pass`, `address`, `number`, `gender`, `skill`, `hobbies`, `dob`, `image`) VALUES
(52, 'S1505230724', 'shahin', 'shahinalam6644@gmail.com', '45', 'Dhaka', '1223', 'Male', 'html', 'cricket', '1,1,1901', '1505230724.jpeg'),
(53, 'S1505238036', 'Shebag', 'sh@mail.com', '12', 'India', '1223', 'Male', 'html', 'cricket', '1,1,1901', '1505238036.jpeg'),
(54, 'S1505238067', 'Mim', 'shahi44@gmail.com', '45', 'Dhaka', '1223', 'Male', 'html', 'football,kabadi', '1,1,1901', '1505238067.png'),
(55, 'S1505238103', 'shjan', 'shahiam6644@gmail.com', '77', 'Dhaka2', '1223', 'Male', 'html', 'football,kabadi', '1,1,1901', '1505238103.jpeg');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
