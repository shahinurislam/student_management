-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Sep 08, 2017 at 08:38 AM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `test2`
--

-- --------------------------------------------------------

--
-- Table structure for table `user_info`
--

CREATE TABLE IF NOT EXISTS `user_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `pass` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `number` varchar(255) NOT NULL,
  `gender` varchar(255) NOT NULL,
  `skill` varchar(255) NOT NULL,
  `hobbies` varchar(255) NOT NULL,
  `dob` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=44 ;

--
-- Dumping data for table `user_info`
--

INSERT INTO `user_info` (`id`, `name`, `email`, `pass`, `address`, `number`, `gender`, `skill`, `hobbies`, `dob`, `image`) VALUES
(19, 'shahin', 'shahinalam6644@gmail.com', 'fff', 'Dhaka', '1223fffffffffff', 'Female', 'html', 'kabadi', '1, 1, 1901', 'file (1).jpeg'),
(21, 'shahin', '01926518051f', 'fsdd', 'f', 'f', 'Male', 'html', 'cricket', '1, 1, 1901', 'businessmann_kl.jpg'),
(27, 'shahin', 'shahin@mail.com', 'pass', 'Dhaka', '0123456', 'Female', 'php, html', 'cricket, football', '11, 3, 1910', 'download.png'),
(31, 'sder', 'ert', 'ert', 'ert', 'wert', 'Male', 'php,css,html', 'cricket,football,kabadi', '1,1,1901', 'download.jpg'),
(41, 'cghf', 'ghf', 'fgh', 'fghf', 'gh', 'Male', 'html', 'cricket', '1,1,1901', 'download.png'),
(43, 'ty', 'tyu', 'tyu', 'ty', 'ty', 'Male', 'html', 'cricket', '1,1,1901', 'download.jpg');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
